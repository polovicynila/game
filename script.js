const start = document.getElementById("start")
const screens = document.querySelectorAll(".screen")
const timeList = document.getElementById("timeList")
const board = document.getElementById("board")
//const time = document.getElementById("time")
let time = 0

const colors = [
  '#48C9B0',
  '#EB984E',
  '#D98880',
  '#52BE80',
  '#52BE80',
  '#2ECC71',
  '#3498DB',
]
start.addEventListener('click', (event) => {
    event.preventDefault()
  screens[0].classList.add('up')
})

timeList.addEventListener('click', (event) => {
  event.preventDefault()
  screens[1].classList.add('up')
})
function createRandomCircle() {
  const circle = document.createElement('div')
  circle.classList.add('circle')
  const size = getRandomNumber(10,60)
  const {width, height} = board.getBoundingClientRect()
  const y = getRandomNumber(0, height-size)
  const x = getRandomNumber(0, width-size)
  circle.style.width = `${size}px`
  circle.style.heigth = `${size}px`
  circle.style.top = `${y}px`
  circle.style.left = `${x}px`
}

function getRandomNumber(min,max) {
return Math.floot(Math.random() * (max-min) + min)
}
function getRandomColor(element){
  const color = getRandomColor()
  element.style.background = color}